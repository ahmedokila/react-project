variable "aws_region" {
  type    = string
  default = "us-east-1"
}
variable "instance_type" {
  type    = string
  default = "t2.micro"
}
variable "ami" {
  type    = string
  default = "ami-080e1f13689e07408"
}
variable "key_name" {
  type    = string
  default = "AWS_EC2"
}