#! Creating the EC2 Instances and Security Groups
resource "aws_security_group" "sg" {
  name        = "myvpc_sg"
  description = "Allow TLS inbound traffic and all outbound traffic"

  tags = {
    Name = "myvpc_sg"
  }
  egress {
    from_port   = 0    # To Indicate any port
    to_port     = 0    # To Indicate any port
    protocol    = "-1" #TCP
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_instance" "instance_1" {
  ami                    = var.ami
  instance_type          = var.instance_type
  vpc_security_group_ids = [aws_security_group.sg.id]
  user_data              = base64encode(file("userdata.sh"))
  key_name               = var.key_name
  tags = {
    Name = "instance_1"
  }
  provisioner "file" {
    source      = "../build/"
    destination = "/tmp"

    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = file("../secret_files/AWS_EC2.pem")
      host        = self.public_ip
    }
  }
}

output "instance_dns_name" {
  value = aws_instance.instance_1.public_dns
}
