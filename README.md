# This is a Fully Automated Project.

### The Implementtion Steps: -
1. Developer pushs his changes
2. The CICD pipeline will be triggered, once it is completed it will output a URL through which the developer can see his changes an running environment.
![Alt text](Images/1.0.JPG)
![Alt text](Images/1.1.JPG)
![Alt text](Images/1.2.JPG)


## The Pipeline Stages: -
### Building the Application and Pushing its Image to Registry: -
- Build
	- Installing the application dependencies
	- Build the react application to produce build directory.
    ![Alt text](Images/1.3.JPG)
- test
	- Running the test cases for the project
    ![Alt text](Images/1.4.JPG)
- dockerize
	- building the image for the application
	- pushing the image to the registry
    ![Alt text](Images/1.5.JPG)
    ![Alt text](Images/1.6.JPG)
	
### Provisioning the Infrastructure and deploying the application on it: -
- init
	- Initializing the backend for terraform scripts.
    ![Alt text](Images/1.7.JPG)
- prepare
	- Injecting some secrets file into the pipeline so that it can make a successfull deployment into the server
    ![Alt text](Images/1.8.JPG)
- plan
	- Dry run for terraform scripts
    ![Alt text](Images/1.9.JPG)
- apply
	- Running the terraform scripts and provisioning the infrastructure.
    ![Alt text](Images/1.10.JPG)
    ![Alt text](Images/5.JPG)

#### The Pipeline will output the DNS name through which the user can access the application with latest changes
![Alt text](Images/1.1.JPG)
![Alt text](Images/1.2.JPG)