### This Project is about to create an Application Load Balancer which will redirect the Traffic to 2 EC2 Instances
#### Implementation Steps: -
```bash
terraform init
```
![Alt text](Images/1.JPG)
```bash
terraform plan
```
![Alt text](Images/2.JPG)
![Alt text](Images/3.JPG)
```bash
terraform apply -auto-approve
```
![Alt text](Images/4.JPG)
![Alt text](Images/5.JPG)
This script will output the DNS of the Load Balancer through which we can access the Instances
![Alt text](Images/6.JPG)
![Alt text](Images/7.JPG)
